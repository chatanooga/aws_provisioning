sudo docker run \
  --rm \
  -u root \
  -v /var/run/docker.sock:/var/run/docker.sock \
  -v /home/ubuntu/jenkins-data:/var/jenkins_home \
  -v "$HOME":/home \
  -e VIRTUAL_HOST=jenkins.pfxr.xyz \
  -e VIRTUAL_PORT=8080 -p 80:80\
  --name paddy_jenkins jenkins/jenkins:lts